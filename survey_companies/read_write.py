﻿''' Functions for Reading/Writing the various files in the survey company matching project

    Generic Functions:
    
        open_workbook: Open an excel workbook that may contain multiple sheets as a dictionary
        
        open_sheet: import a worksheetfrom an Excel XML/HTML/CSV/TXT format as a pandas dataframe
        
        remove_pre_header_rows: If a worksheet  has one or more extraneous rows before the header, drop them
        
    Project Specific Functions:
    
        load_master: import the master list from an Excel XML/HTML/CSV/TXT format into a pandas dataframe
        
        load_survey: import the survey list(s) from an Excel XLSX format into a {dict} of pandas dataframes
        
        load_dictionary: load a dictionary word list file into a set to be used for filtering
        
        read_output_validations: Read the matching confirmations that a user entered to accept/reject a specific match
        
        store_validations: write saved validations out to a worksheet so future matching will be improved
        
        load_validations: read saved validations from a worksheet file to improve matching
        
'''

import os, sys, re
from time import time
from collections import defaultdict, Counter
import pandas as pd
import xlrd

i_MAX_INVALIDS = 10

c_COMPANY_NAME_COLS = {
    "company name", "company", "companies",
    "participant", "participants"}
s_VALIDATION_COMPANY_COL = "Account Name"
s_VALIDATION_ALTERNATE_COL = "Alternate"
s_VALIDATION_CONFIRM_COL = "Confirm"
s_VALIDATION_VERSION_COL = "Version"


##### GENERIC UTILITY FUNCTIONS

def open_workbook(s_path):
    ''' Open an excel workbook that may contain multiple sheets and
        Arguments:
            s_path: {str} of the path to the xlsx workbook to load
        Returns:
            {dict} of {sheet_name: pd.DataFrame} '''
    o_xl = pd.ExcelFile(s_path)
    e_sheets = {}
    for s_sht in o_xl.sheet_names:  # see all sheet names
        e_sheets[s_sht] = o_xl.parse(sheet_name=s_sht)
    return e_sheets


def open_sheet(s_path, **kwargs):
    ''' import a worksheetfrom an Excel XML/HTML/CSV/TXT format as a pandas dataframe
        Arguments:
            s_path: {str} of the path to the excel file to load
        Returns:
            {pd.DataFrame} of the worksheet
    '''
    s_folder, s_file = os.path.split(s_path)
    s_filename, s_ext = os.path.splitext(s_file)
    if s_ext.lower() in {"", ".xlsx", ".xlsm", ".xls"}:
        try:
            if kwargs.get("sheet_name"):
                df_sheet = pd.read_excel(s_path, sheet_name=kwargs.get("sheet_name"))
            else:
                df_sheet = pd.read_excel(s_path)
        except xlrd.XLRDError:
            a_master = pd.read_html(s_path)
            df_sheet = a_master[0]
    elif s_ext.lower() in {".csv"}:
        try:
            df_sheet = pd.read_csv(s_path, sep=kwargs.get("sep", ","))
        except pd.errors.ParserError:
            a_master = pd.read_html(s_path)
            df_sheet = a_master[0]
    elif s_ext.lower() in {".tab", ".txt"}:
        try:
            df_sheet = pd.read_excel(s_path, sep=kwargs.get("sep", "\t"))
        except pd.errors.ParserError:
            a_master = pd.read_html(s_path)
            df_sheet = a_master[0]
    return df_sheet


def remove_pre_header_rows(df, header_cols, i_max_pre_header_rows=10, b_copy=True):
    '''
        If a worksheet  has one or more extraneous rows before the header, drop them
        Arguments:
            df: {pd.DataFrame}
            header_cols: {set} of columns to search for that identify the header row
            i_max_pre_header_rows: {int} the maximum number of rows to look through for header columns before giving up
            b_copy: {bool} whether to return a copy of the dataframe rather than altering the one passed in
        Returns:
            {pd.DataFrame}
    '''
    header_vals = set([x.lower().strip() for x in df.columns]).intersection(header_cols)
    if header_vals: return df
    i_header_row, a_labels = -1, None

    for i_row, t_row in enumerate(df.itertuples()):
        a_labels = list(t_row[1:])
        header_vals = set([x.lower().strip() for x in t_row if isinstance(x, str)]).intersection(header_cols)
        if header_vals:
            i_header_row = i_row
            break
    #print(i_header_row, a_labels)
    if a_labels:
        df.columns = a_labels
        df = df[i_header_row + 1:]

    if b_copy:
        return df.copy()
    else:
        return df

##### SPECIFIC #####

def load_master(s_path, **kwargs):
    ''' import the master list from an Excel XLSX/XML/HTML/CSV/TXT format into a pandas dataframe
        Arguments:
            s_path: {str} of the path to the excel file to load
            a_industries: {list} of values that a company must have in at
                          least one of the industry columns to be included in the output
            a_sic_codes: {list} of values that a company must have in
                         the SIC Code column to be included in the output
        Returns:
            {pd.DataFrame} of the companies that match the specified industries/SIC codes
    '''
    s_company_name_col = kwargs.get("s_company_name_col", "Account Name")
    s_folder, s_file = os.path.split(s_path)
    df_master = open_sheet(s_file, **kwargs)
    df_master.dropna(subset=[s_company_name_col], inplace=True)
    df_master[s_company_name_col] = df_master[s_company_name_col].astype(str).str.strip()
    i_rows = len(df_master)
    df_master.drop_duplicates(subset=[s_company_name_col], inplace=True)
    i_companies = len(df_master)
    i_dupes = i_rows - i_companies
    print(f"{i_companies:,} companies in master company file in {s_file} ({i_dupes} duplicates removed)")
    return df_master


def load_survey(s_path, **kwargs):
    ''' import the survey list(s) from an Excel XLSX format into a {dict} of pandas dataframes
        Arguments:
            s_path: {str} of the path to the excel file to load
            a_industries: {list} of values that a company must have in at
                          least one of the industry columns to be included in the output
            a_sic_codes: {list} of values that a company must have in
                         the SIC Code column to be included in the output
        Returns:
            {pd.DataFrame} of the companies that match the specified industries/SIC codes
    '''
    s_folder, s_file = os.path.split(s_path)
    if s_file.lower().endswith((".xlsx", ".xlsm", ".xls")):
        if kwargs.get("sheet_name"):
            e_surveys = {kwargs["sheet_name"]: open_sheet(s_path, **kwargs)}
        elif len(kwargs.get("sheet_names", [])) == 1:
            e_surveys = {kwargs["sheet_names"][0]: open_sheet(s_path, sheet_name=kwargs["sheet_names"][0], **kwargs)}
        else:
            e_surveys = open_workbook(s_path)
            if kwargs.get("sheet_names"):
                e_surveys = {s_sht: df for s_sht, df in e_surveys.items() if s_sht in kwargs["sheet_names"]}
    else:
        s_filename, s_ext = os.path.splitext(s_file)
        e_surveys = {s_filename: open_sheet(s_path, **kwargs)}
            
    print(f"{len(e_surveys.keys())} survey sheets in {s_file}")
    
    e_survey_companies = defaultdict(set)
    for s_sheet, df_participants in e_surveys.items():
        if 1 <= df_participants.shape[1] <= 4:
            print(f'"{s_sheet}": {df_participants.shape[0]} rows, {df_participants.shape[1]} cols')
            for col in df_participants:
                a_vals = []
                for s_val in df_participants[col].dropna().astype(str).str.strip():
                    for s_piece in re.split(r"[*▪|]", s_val):
                        s_piece = re.sub(r"(^[-_*▪ ]+|[-_*▪ ]+$)", "", s_piece).strip()
                        if s_piece: a_vals.append(s_piece)
                i_vals = len(a_vals)
                e_vals = Counter(a_vals)
                e_dupes = {k: v for k, v in e_vals.items() if v > 1}
                i_unique_vals = len(set(e_vals))
                i_dupes = len(e_dupes)
                d_unique = i_unique_vals / i_vals if i_vals else 0.0
                print("\t", f"Values:{i_vals:,}, Unique:{i_unique_vals:,}, Uniqueness:{d_unique:0.1%}, Duplicates:{i_dupes:,}, \"{col}\".", e_dupes)
                
                if d_unique > 0.9:
                    e_survey_companies[s_sheet].update(list(e_vals.keys()))
        else:
            df_participants = remove_pre_header_rows(df_participants, c_COMPANY_NAME_COLS)
            print(f'"{s_sheet}": {df_participants.shape[0]} rows, {df_participants.shape[1]} columns') #, list(df_participants.columns))
            for col in df_participants.columns:
                if col.lower() not in c_COMPANY_NAME_COLS: continue
                a_vals = [ re.sub(r"(^[-_* ]+|[-_* ]+$)", "", col)] + [
                    re.sub(r"(^[-_* ]+|[-_* ]+$)", "", x) for x in 
                    df_participants[col].dropna().astype(str).str.strip()]
                
                i_vals = len(a_vals)
                e_vals = Counter(a_vals)
                e_dupes = {k: v for k, v in e_vals.items() if v > 1}
                i_unique_vals = len(set(e_vals))
                i_dupes = len(e_dupes)
                d_unique = i_unique_vals / i_vals if i_vals else 0.0
                print("\t", f"Values:{i_vals:,}, Unique:{i_unique_vals:,}, Uniquness:{d_unique:0.1%}, Duplicates:{i_dupes:,}, \"{col}\".", e_dupes)
                
                e_survey_companies[s_sheet].update(list(e_vals.keys()))
    return e_survey_companies


def load_dictionary(s_path):
    ''' load a dictionary word list file into a set to be used for filtering '''
    print(os.path.join(os.path.split(__file__)[0], "data", s_path))
    if not os.path.isfile(s_path) and os.path.isfile(os.path.join(os.path.split(__file__)[0], "data", s_path)):
        s_path = os.path.join(os.path.split(__file__)[0], "data", s_path)
    df_word_list = pd.read_csv(s_path, header=None)
    c_WORDS = set([word.lower() for word in df_word_list[0].dropna().astype(str).values])
    print(f"{len(c_WORDS):,} words in {s_path}")
    return c_WORDS


def read_output_validations(s_output_file_with_validations, e_validations=None, e_valids=None, e_invalids=None, **kwargs):
    ''' Read the matching confirmations that a user entered to accept/reject a specific match
        Arguments:
            s_output_file_with_validations: {str} of path output file that user has entered "Confirm 1", "Confirm 2"... values into
            e_validations: optional {dict} of already loaded validations to add new validations too
            e_valids: optional {dict} of already loaded valid matches to add new matches too
            e_invalids: optional {dict} of already loaded invalid matches for each to add new incorrect matches too
        Returns:
            {tuple} of dicts (e_validations, e_valids, e_invalids)
    '''
    if e_validations is None:
        e_validations = defaultdict(lambda: {"valid": set(), "invalid": set(), "raw": {}})
    if e_valids is None: e_valids = {}
    if e_invalids is None: e_invalids = defaultdict(set)
    
    df_feedback = open_sheet(s_output_file_with_validations)
    
    for i_match in range(10):
        if f"Confirm {i_match}" not in df_feedback.columns: continue
        print(f"Checking validations of 'Matched Company {i_match}' using 'Confirm {i_match}'")
        a_cols = [f"Company", f"Search Version {i_match}", f"Matched Company {i_match}", f"Match Version {i_match}", f"Confirm {i_match}"]
        df_val_set = df_feedback[a_cols].dropna(subset=[f"Matched Company {i_match}", f"Confirm {i_match}"])
        for idx, s_company, s_search_version, s_matched_company, s_matched_version, v_validation in df_val_set.itertuples():
            for s_match_comp in s_matched_company.split("|"):
                s_comp_lower = s_company.lower()
                s_match_comp = s_match_comp.lower().strip()
                
                if not pd.isnull(v_validation):
                    e_validations[s_comp_lower]["raw"][s_match_comp] = v_validation
                
                i_valid = 0
                if isinstance(v_validation, str):
                    if v_validation.lower() in {"valid", "confirmed", "true"}:
                        i_valid = 1
                    elif v_validation.lower() in {"invalid", "disproved", "false"}:
                        i_valid = -1
                elif isinstance(v_validation, bool):
                    if v_validation:
                        i_valid = 1
                    elif not v_validation:
                        i_valid = -1
                else:
                    if v_validation >= 1:
                        i_valid = 1
                    elif v_validation <= -1:
                        i_valid = -1
                    
                if i_valid >= 1:
                    e_valids[s_comp_lower] = s_match_comp
                    e_valids[s_match_comp] = s_comp_lower
                    e_validations[s_comp_lower]["valid"].add(s_match_comp)
                    e_validations[s_match_comp]["valid"].add(s_comp_lower)
                elif i_valid <= -1:
                    e_invalids[s_comp_lower].add(s_match_comp)
                    e_invalids[s_match_comp].add(s_comp_lower)
                    e_validations[s_comp_lower]["invalid"].add(s_match_comp)
                    e_validations[s_match_comp]["invalid"].add(s_comp_lower)
                    c_invalids.add((s_comp_lower, s_match_comp))
    
    #print(len(e_valids), len(e_invalids))
    return e_validations, e_valids, e_invalids


def store_validations(e_validations, e_mapping_kinds, e_mappings=None, df_validations=None, s_output="", **kwargs):
    ''' write saved validations out to a worksheet so future matching will be improved
        Arguments:
            e_validations: {dict} of already loaded validations to save
            e_mapping_kinds: {dict} of the version types of company values
                {company_name_version1: ("original" or "clean" or "abbr_company_words" or "removed_company_words" or "acronym")}
            e_mappings: {dict} the master company names that each version maps to
            df_validations: optional {pd.DataFrame} table or {str} path to excel file of existing saved file
            s_output: optional {str} path to write the new file to
        Returns:
            {pd.DataFrame} of the table of saved validations
    '''
    
    e_validated_rows = {}
    if df_validations is not None:
        if isinstance(df_validations, str):
            if df_validations.lower().endswith(".csv"):
                df_validations = pd.read_csv(df_validations, sep=kwargs.get("sep", ","))
            elif df_validations.lower().endswith(".txt"):
                df_validations = pd.read_csv(df_validations, sep=kwargs.get("sep", "\t"))
            elif df_validations.lower().endswith((".xls", ".xlsx", ".xlsm")):
                try:
                    df_validations = pd.read_excel(df_validations, sheet_name=kwargs.get("sheet_name"))
                except xlrd.XLRDError:
                    a_prior = pd.read_html(df_validations)
                    df_validations = a_prior[0]
        
        if isinstance(df_validations, pd.DataFrame) and len(df_validations) > 0:
            for t_row in df_validations.itertuples():
                e_validated_rows[t_row[1:3]] = t_row[1:]
    
        print(f"Prior validated rows: {len(e_validated_rows):,}")
    
    if e_mappings:
        for s_version, a_comps in e_mappings.items():
            s_kind = e_mapping_kinds.get(s_version, "alternate")
            for s_comp in a_comps:
                if s_comp.lower() == s_version: continue
                e_validated_rows[(s_comp.lower(), s_version)] = (s_comp, s_version, None, s_kind)
    
    if e_validations:
        for s_comp, e_val in e_validations.items():
            s_comp_lower = s_comp.lower()
            
            for s_valid in e_val.get("valid", []):
                s_kind = e_mapping_kinds.get(s_valid, "alternate")
                e_validated_rows[(s_comp_lower, s_valid)] = (s_comp, s_valid, 1, s_kind)
            if len(e_val.get("invalid", [])) >= i_MAX_INVALIDS:
                e_validated_rows[(s_comp_lower, None)] = (s_comp, None, -1, "all")
            else:
                for s_invalid in e_val.get("invalid", []):
                    s_kind = e_mapping_kinds.get(s_invalid, "alternate")
                    e_validated_rows[(s_comp_lower, s_invalid)] = (s_comp, s_invalid, -1, s_kind)
    
    df_validated = pd.DataFrame(
        e_validated_rows.values(),
        columns=[
            s_VALIDATION_COMPANY_COL, s_VALIDATION_ALTERNATE_COL, s_VALIDATION_CONFIRM_COL, s_VALIDATION_VERSION_COL])
    
    if s_output:
        if s_output.lower().endswith(".csv"):
            df_validated.to_csv(s_output, encoding=kwargs.get("encoding", "utf-8-sig"), index=False)
        elif s_output.lower().endswith(".txt"):
            df_validated.to_csv(s_output, encoding=kwargs.get("encoding", "utf-8-sig", sep=kwargs.get("sep", "\t")), index=False)
        elif s_output.lower().endswith(".xlsx"):
            df_validated.to_excel(s_output, sheet_name=kwargs.get("sheet_name", "matches"), index=False)
    
    return df_validated


def load_validations(df_validations, e_validations=None, e_mappings=None, e_mapping_kinds=None, e_valids=None, e_invalids=None, **kwargs):
    ''' read saved validations from a worksheet file to improve matching
        Arguments:
            df_validations: {pd.DataFrame} table or {str} path to excel file of existing saved file
            
            e_validations: {dict} of already loaded validations to add to saved matches to
            e_mappings: {dict} of mappings from alternates to their company names, regardless of validation
            e_mapping_kinds: {dict} of the version types of company values to add to saved matches to
                {company_name_version1: ("original" or "clean" or "abbr_company_words" or "removed_company_words" or "acronym")}
            
            e_valids: optional {dict} of already loaded valid matches to add saved matches too
            e_invalids: optional {dict} of already loaded invalid matches for each to add new incorrect matches too
        
        Returns:
            {tuple} of dicts (e_validations, e_mapping_kinds, e_valids, e_invalids)
    '''
    if e_validations is None:
        e_validations = defaultdict(lambda: {"valid": set(), "invalid": set(), "raw": {}})
    
    if e_mappings is None: e_mappings = defaultdict(list)
    if e_mapping_kinds is None: e_mapping_kinds = {}
    if e_valids is None: e_valids = {}
    if e_invalids is None: e_invalids = defaultdict(set)
    
    e_validated_rows = {}
    assert df_validations is not None
    
    if isinstance(df_validations, str):
        if df_validations.lower().endswith(".csv"):
            df_validations = pd.read_csv(df_validations, sep=kwargs.get("sep", ","))
        elif df_validations.lower().endswith(".txt"):
            df_validations = pd.read_csv(df_validations, sep=kwargs.get("sep", "\t"))
        elif df_validations.lower().endswith((".xls", ".xlsx", ".xlsm")):
            try:
                df_validations = pd.read_excel(df_validations, sheet_name=kwargs.get("sheet_name", "matches"))
            except xlrd.XLRDError:
                a_prior = pd.read_html(df_validations)
                df_validations = a_prior[0]

    if isinstance(df_validations, pd.DataFrame) and len(df_validations) > 0:
        df_validations = df_validations.dropna(subset=[s_VALIDATION_COMPANY_COL, s_VALIDATION_ALTERNATE_COL], how="any")
        df_validations[s_VALIDATION_COMPANY_COL] = df_validations[s_VALIDATION_COMPANY_COL].astype(str)
        df_validations[s_VALIDATION_ALTERNATE_COL] = df_validations[s_VALIDATION_ALTERNATE_COL].astype(str)
        
        for t_row in df_validations.itertuples():
            #e_validations[t_row[1]]["raw"][t_row[2]] = tuple(t_row[1:])
            if pd.isnull(t_row[2]) or t_row[4] in {"ALL", "all"}:
                t_row = t_row[:2] + (None,) + t_row[3:]
                #print(t_row)
            
            e_mappings[t_row[2]].append(t_row[1])
            e_mappings[t_row[1].lower()].append(t_row[1])
            e_mapping_kinds[t_row[1].lower()] = "original"
            
            if not pd.isnull(t_row[3]):
                e_validations[t_row[1]]["raw"][t_row[2]] = t_row[3]
            
            i_valid = 0
            if isinstance(t_row[3], str):
                if t_row[3].lower() in {"valid", "confirmed", "true"}:
                    i_valid = 1
                elif t_row[3].lower() in {"invalid", "disproved", "false"}:
                    i_valid = -1
            elif isinstance(t_row[3], bool):
                if t_row[3]:
                    i_valid = 1
                elif not t_row[3]:
                    i_valid = -1
            else:
                if t_row[3] >= 1:
                    i_valid = 1
                elif t_row[3] <= -1:
                    i_valid = -1
                
            if i_valid >= 1:
                e_validations[t_row[1]]["valid"].add(t_row[2])
                e_valids[t_row[1]] = t_row[2]
                e_valids[t_row[2]] = t_row[1]
            elif i_valid <= -1:
                e_validations[t_row[1]]["invalid"].add(t_row[2])
                e_invalids[t_row[1]].add(t_row[2])
                e_invalids[t_row[2]].add(t_row[1])
                    
            
            if isinstance(t_row[4], str) and t_row[4] and t_row[4] != "alternate":
                e_mapping_kinds[t_row[2]] = t_row[4]
    
    return e_validations, e_mappings, e_mapping_kinds, e_valids, e_invalids