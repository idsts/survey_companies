import os, sys, re
from time import time
from functools import reduce
from collections import defaultdict, Counter
from operator import itemgetter
import pandas as pd

try:
    from fuzzy_matcher import fuzzy_matcher
    from fuzzy_matcher.fuzzy_matcher import *
except:
    import fuzzy_matcher
    from fuzzy_matcher import *

try:
    from . import read_write
except:
    import read_write


''' Prep the master company list to load into the fuzzy_matcher
    functions:
    
        _load_regexes
        
        secondsToStr: convert seconds into a human-readable time string
        
        herfindahl_freqs_dict: Calculate the HHI from a frequency dictionary
        
        freq_dict_to_percent: Convert a counter dictionary containing frequencies into percentage of total like a softmax function
        
        ordinal: Convert an integer into its respect ordinal. 1 -> "1st", 2 -> "2nd", etc.
        
    class CompanyMatcher:
        Methods:
            __init__: Load necessary variables/libraries/files to prepare for company matching
            
            create_fuzzy_matcher: Load a fuzzy_matcher.fuzzy_texts.FuzzyTexts class instance with master company versions
            
            process_survey: Load in a survey file with company names and try to find the best matches for each of them
            
            output_results: Turn the output matches into a dataframe for export to excel
            
            get_report: Generate a report sheet on the scores of the matches, and concentrations of industry/sic codes
        
            analyze_concentrations: Analyze the concentrations of industries and sic codes using the frequencies found with the matches
        
            get_industry_companies: Get the list of companies to search against by filtering with industries or SIC Codes
            
            clean_company_words: Remove or substitute words from the company text by splitting into words and then checking if it should be substituted/deleted
            
            make_company_acronym: Try to make an acronym from a company name as an additional fuzzy match alternative for long company names
            
            get_name_versions: Create the unique versions of the company name with standardized punctuation, abbr/removed common company words, acronym.
            
            get_company_name_versions: Assemble the different version of each companies name from master db to go into the fuzzy matcher
            
            disambiguate_kinds: pick the least valuable kind for e_mapping_kinds of a single version might be several of them
            
            get_matches: Find the matches for a single company.
            
            get_confirmed_matches: Find any matches that were already confirmed as good in prior runs
            
            get_version_matches: Find any exact matches between the versions generated from the searched company and versions of companys in the master db
            
            get_fuzzy_matches: Find any fuzzy matches between the search company and companies in the master db
            
            dedupe_matches: Deduplicate matches pointing to the same mapped company
            
            filter_disproved_matches: Filter out any matches from a list where the mapped company has already been marked as invalid by a human
            
            filter_metadata_matches: Filter out or reduce the score of matches where the mapped company is not in the specified industries or sic codes
'''
i_OUTPUT_MATCHES = 3
i_MOST_COMMON = 2
s_DB_COMPANY_COL = "Account Name"
s_DB_SIC_CODE_COL = "SIC Code"
a_INDUSTRY_COLS = ["Industry", "Primary Industry (Zoom)", "Sub Industry (Zoom)"]
a_METADATA_COLS = a_INDUSTRY_COLS + [s_DB_SIC_CODE_COL]
a_MASTER_COLS_TO_ADD = ["Industry", s_DB_SIC_CODE_COL]


c_COMPANY_ABBREVIATIONS = {
    'company', "co", "comp",
    'corporation', "corp",
    'incorporated', "inc",
    'limited', "ltd", "lmtd", "lte", "ltee",
    'international', "intl", "int'l",
    'association', "assn", "assoc", 'associates',
    "pa", "pc",
    "lp", "llp", "lllp", "lc", "llc",
    "partners", "ptnrs",
    "bank",
}
c_SKIP_COMPANY_ABBREVIATIONS = {  #always skip
    "pa", "pc",
    "lp", "llp", "lllp", "lc", "llc", "ltd",
}

c_SKIP_ACRONYM_WORDS = {
    "the", "of", 
}    

a_COMMON_COMPANY_WORDS = [
    # ("group type", "subsitution", b_keep, [members]),
    ("punctuation", "", False, ["&", "+", "-", "/", "\\"]),
    ("llc", "llc", False, [
        "llc", "lllc", "lc", "l.l.c.", "l.l.l.c.", "l.c.", "l.l.c", "l.l.l.c", "l.c",
        "limited liability corporation", "limited liability company", "limited company",
        "limited liability co", "limited co",
        "ltd liability corporation", "ltd corporation", "ltd liability co", "ltd co",
        "lte", "ltee", "lte", "ltee", ]),
    ("llp", "llp", False, [
        "llp", "lllp", "lp", "l.l.p.", "l.l.l.p.", "l.p.", "l.l.p", "l.l.l.p", "l.p",
        "limited liability partnership", "limited liability partnership", "limited partnership",
        "liability",
        "lte", "ltee", "l.t.e.", "l.t.e.e.", "l.t.e", "l.t.e.e",
        "limited liability enterprise", "limited liability entity", "limited enterprise",
    ]),
    ("company", "co", False, [
        "co", "cos", "comp", "cmp", "cmps", "co.", "cos.", "comp.", "cmp.", "cmps.", "cmpny", "company", "companies"]),
    ("corporation", "corp", False, ["corporation", "corp", "sa", "ag", "corp.", "s.a.", "a.g."]),
    ("incorporated", "inc", False, ["incorporated", "inc", "inc.", "incorporatd", "incorporation"]),
    ("limited", "ltd", False, ["limited", "ltd", "lmtd", "ltd.", "lmtd.",]),
    ("pro_corp", "pc", False, [
        "pc", "psc", "p.c.", "p.s.c.", "p.c", "p.s.c",
        "professional corporation", "professional corp", "pro corporation", "pro corp",
        "professional service corporation", "professional service corp", "pro service corporation", "pro svc corp",
        "professional service", "professional services",
    ]),
    ("pro_assoc", "pa", False, [
        "pa", "p.a.", "p.a",
        "professional association", "professional assoc", "professional assn"]),
    ("group", "grp", False, ["group", "gruppe", "firmengruppe"]),

    ("system", "sys", True, ["system", "systems"]),
    ("association", "assn", True, ["association", "associations"]),
    ("international", "intl", True, ["international"]),
    ("management", "mngt", True, ["management", "gestion", "gérance"]),
    ("services", "svcs", True, ["services", "service", "personal-services", "personaldienstleistungen",]),
    ("real estate", "rlty", True, ["real estate", "realtors", "realtor", "realty", "realties", "immobilier", "immobiliers", "immobilien", "immobilière"]),
    ("agency", "agt", True, ["agent", "agentin", "agence", "agency", "agentur", "l'agence"]),
    ("restaurant", "rest", True, ["restaurant", "ristorante"]),
    ("partner", "ptnr", True, ["partner", "partners", "ptnr", ]),
    ("associate", "assoc", True, ["associate", "associates", "assoc", "associés", "ahnliche",]),
    ("contractor", "cont", True, ["contractor", "contractors", "generalunternehmung",]),
    ("solutions", "sol", True, ["solutions", "solution", "sol", "lösungen", "lösung"]),
    ("operations", "ops", True, ["operations", "ops", "operation",]),
    ("insurance", "vers", True, ["insurance", "versicherung"]),
    ("distribution", "dist", True, ["distribution", "verteilung"]),
    ("productions", "prdtn", True, ["productions", "production", "produktionen"]),
    ("garage", "grge", True, ["garage", "garages"]),
    ("manufacture", "mfr", True, ["manufacture", "manufacturer", "herstellung", "herst",]),
    ("inst", "inst", True, ["institut", "institute", "inst",]),
    ("logistics", "logi", True, ["logistics", "logistik", "logistic"]),
    ("landscapers", "lndsc", True, ["landscapers", "landscaper", "landscapes", "landscaping", "paysagistes", "landschaftsgestalter", "landschaftsbau"]),
    ("lab", "lab", True, ["laboratory", "laboratoire", "labor", "laboratorium", "laboratories"]),
    ("foundation", "fdn", True, ["fondation", "foundation", "stiftung", "gründung"]),
    #("baumanagement", "baumgt", True, ["baumanagement", "construction-management"]),
    ("construction", "cnstrct", True, ["bau", "construction"]),
    ("consultant", "cnsult", True, ["consultant", "consultants", "consultancy", "consulting", "personalberatung", "conseil", "unternehmensberatung"]),
    ("university", "univ", True, ["university", "universität", "université"]),
    ("architects", "arch", True, ["architect", "architects", "architecture", "architektur"]),
    ("express", "expr", True, ["express"]),
    ("engineering", "engr", True, ["engineering", "engineer", "engineers", "ingenieurwesen", "ingenieur", "ingenieurin", "ingenieure", "ingenieurinnen"]),
    ("municipality", "muni", True, ["gemeindeverwaltung", "gemeinde", "community", "einwohnergemeinde", "municipality"]),
    ("communication", "comm", True, ["communication", "communications", "kommunikation"]),
    ("technology", "tech", True, ["technology", "technologies", "technological", "tech", "tech."]),
    ("transportation", "trans", True, ["transportation", "transports", "transport", "trans"]),
    ("brands", "brnd", True, ["brands", "brand", "brnd"]),
    ("national", "natl", True, ["national", "natl", "nat'l"]),
    ("america", "amer", True, ["america", "amer", "unites states of america"]),
    ("canada", "can", True, ["canada", "can"]),
    ("products", "prod", True, ["products", "product", "prod"]),
    ("packaging", "pkg", True, ["packaging", "pkg",]),
    ("industries", "ind", True, ["industries", "industry", "indstr"]),
    ("bank", "bnk", True, ["bank", "bnk", "banks", "banking", "bancorp"]),
    ("financial", "fin", True, ["financial", "financials", "finances", "finance", "financing", "financings", "fin"]),
    ("mortgage", "mrtg", True, ["mortgage", "mrtg", ""]),
    ("credit", "cr", True, ["credit", "cr", ""]),
    #("union", "un", True, ["union", "un", ""]),
    ("investments", "inv", True, ["investments", "investment", "inv"]),
    ("studio", "studio", True, ["studio", "studios"]),
    ("network", "net", True, ["networks", "network", "net"]),
    
    #("", "", True, ["", "", ""]),
]
c_COMPANY_GROUP_ABBRS = {t[1].lower() for t in a_COMMON_COMPANY_WORDS}
c_COMPANY_GROUPS = {t[1].lower() for t in a_COMMON_COMPANY_WORDS}
c_COMPANY_GROUPS.update({word.lower() for t in a_COMMON_COMPANY_WORDS for word in t[3]})

e_VERSION_SCORES = {
    "confirmed": 1.0,
    "original": 0.99,
    "clean": .95,
    "abbr_company_words": .9,
    "removed_company_words": 0.75,
    "acronym": 0.5,
}

e_TYPE_FACTORS = {
    "confirmed": 1.0,
    "original": 1.0,
    "clean": .95,
    "abbr_company_words": .8,
    "removed_company_words": 0.75,
    "acronym": 0.5,
}
e_TYPE_EXP = {
    "confirmed": .5,
    "original": .7,
    "clean": .8,
    "abbr_company_words": 1.1,
    "removed_company_words": 1.5,
    "acronym": 1.5,
}


e_SUBS = {
    word.lower(): substitute
    for group, substitute, keep, words in a_COMMON_COMPANY_WORDS
    for word in words}
c_DELS = {
    word.lower()
    for group, substitute, keep, words in a_COMMON_COMPANY_WORDS
    for word in words
    if not keep}

#a_COMPANY_PHRASES = []
e_COMPANY_REGEXES = {}


def _load_regexes():
    global e_COMPANY_REGEXES
    e_COMPANY_REGEXES = {}
    for s_type, s_val, b_keep, a_ver in a_COMMON_COMPANY_WORDS:
        a_phrases = [s_ver for s_ver in a_ver if re.search("[ .]", s_ver)]
        #if a_phrases: a_COMPANY_PHRASES.append((s_type, s_val, a_phrases))
        s_pattern = r"(^|\b)({})(\b|$)".format("|".join([re.escape(s_phr).replace(r"\\ ", " *") for s_phr in a_phrases]))
        if a_phrases: e_COMPANY_REGEXES[s_val] = re.compile(s_pattern, flags=re.I)
_load_regexes()


class CompanyMatcher:
    def __init__(self, master_company_list, english_dict="common_words.txt", saved_validations=None, prior_output_file=None, **kwargs):
        '''
            Load necessary variables/libraries/files to prepare for company matching.
            Read prior validations, reads master company db and creates alternate versions of each company and
            maps them to the original company name in the master db.
            Arguments:
                master_company_list: {str} path to master company list excel file
                english_dict: {str} path to dictionary text file with list of english words that can safely be abbreviated in acronyms
                saved_validations: {str} path to excel/csv/txt file that has saved validations written in it
                prior_output_file: {str} path to excel file from prior run that a person has validated matches in
        '''
        
        self._validations = defaultdict(lambda: {"valid": set(), "invalid": set(), "raw": {}})
        self._mappings = defaultdict(list)
        self._mapping_kinds = {}
        self._valids, self._invalids = {}, defaultdict(set)
        
        self._fuzzy_matcher = None
        self._common_english = read_write.load_dictionary(english_dict)
        
        if saved_validations:
            self._validations, self._mappings, self._mapping_kinds, self._valids, self._invalids = read_write.load_validations(
                saved_validations,
                e_validations=self._validations,
                e_mappings=self._mappings,
                e_mapping_kinds=self._mapping_kinds,
                e_valids=self._valids,
                e_invalids=self._invalids,
                **kwargs)
        
        
        if prior_output_file:
            self._validations, self._valids, self._invalids = read_write.read_output_validations(
                prior_output_file,
                e_validations=self._validations,
                e_valids=self._valids,
                e_invalids=self._invalids,
                **kwargs)
        
        #Load the master list of companies
        self._master_db = read_write.load_master(master_company_list, s_company_name_col=s_DB_COMPANY_COL, **kwargs)
        
        self.industries = kwargs.get("industries", [])
        self.sic_codes = kwargs.get("sic_codes", [])
        self._industries = set([x.lower().strip() for x in self.industries if x.strip()])
        self._sic_codes = set([x.lower().strip() for x in self.sic_codes if x.strip()])
        
        self._company_metadata = {}
        if self.industries or self.sic_codes:
        
            if a_METADATA_COLS:
                self._company_metadata = {
                    s_company: {
                        k: v.lower().strip() if isinstance(v, str) else v
                        for k, v in e_comp_metadata.items()
                        if not pd.isnull(v)}
                    for s_company, e_comp_metadata in
                    self._master_db.set_index(s_DB_COMPANY_COL)[a_METADATA_COLS].to_dict(orient="Index").items()}
            
            if kwargs.get("b_only_filtered_matches"):
                #Filter the list down to just the selected Industries/SIC Codes
                self._master_db = self.get_industry_companies(
                    self._master_db,
                    a_industries=self.industries,
                    a_sic_codes=self.sic_codes,
                    **kwargs)
        
        a_potential_problem_companies = sorted(
            [
                x for x in self._master_db[s_DB_COMPANY_COL]
                if x.isupper() and len(re.sub(r"[-_+ &.,]", "", x)) <= 4],
            key=lambda x: len(re.sub(r"[-_+ &.,]", "", x)),
            reverse=False)
        print(f"There are {len(a_potential_problem_companies):,} companies in the DB that are suspiciously short. They may be acronyms or missing the company type word.")
        if kwargs.get("b_debug"):
            for s_prob in a_potential_problem_companies:
                print(r"\t{s_prob}")
        
        self._companies, self._mappings, e_mapping_kinds = self.get_company_name_versions(self._master_db, b_skip_company_types=False)
        
        self._unambiguous_mappings = {
            s_alt: a_companies
            for s_alt, a_companies in self._mappings.items()
            if len(a_companies) == 1}
        
        self._ambiguous_mappings = {
            s_alt: a_companies
            for s_alt, a_companies in self._mappings.items()
            if len(a_companies) > 1 and e_mapping_kinds[s_alt] - {'acronym'}}
        
        print(f"{len(self._companies):,} companies, {len(self._mappings):,} versions")
        print(f"{len(self._unambiguous_mappings):,} unambiguous versions, {len(self._ambiguous_mappings):,} ambiguous versions")
        
        self._mapping_kinds.update(self.disambiguate_kinds(e_mapping_kinds))
        
        self._survey_companies = None
    
    def create_fuzzy_matcher(self, **kwargs):
        ''' Load a fuzzy_matcher.fuzzy_texts.FuzzyTexts class instance with all
            the versions from company names from the master db '''
        self._fuzzy_matcher = fuzzy_matcher.FuzzyTexts(
            self._mappings.keys(),
            i_min_ngram_chars = kwargs.get("i_min_ngram_chars", 3),
            i_max_ngram_chars = kwargs.get("i_max_ngram_chars", 12),
            i_min_matches = kwargs.get("i_min_matches", 10),
            i_max_matches = kwargs.get("i_max_matches", 25),
            i_max_start_len = kwargs.get("i_max_start_len", 15),
            i_max_end_len = kwargs.get("i_max_end_len", 15),
            d_length_weight = kwargs.get("d_length_weight", 0),
            d_edit_weight = kwargs.get("d_edit_weight", 1.25),
            d_ngram_weight = kwargs.get("d_ngram_weight", 1.5),
            d_phonetic_weight = kwargs.get("d_phonetic_weight", 0.25),
            d_start_weight = kwargs.get("d_start_weight", 0.5),
            d_end_weight = kwargs.get("d_end_weight", 0.05),
            d_number_weight = kwargs.get("d_number_weight", 0.5),
            d_contains_search_weight = kwargs.get("d_contains_search_weight", 0.75),
            d_contains_match_weight = kwargs.get("d_contains_match_weight", 0.5),
            a_unimportant_words = list(kwargs.get("a_unimportant_words", c_COMPANY_ABBREVIATIONS)),
            a_less_important_words = list(kwargs.get("a_less_important_words", c_COMPANY_GROUPS)),
            d_important_word_factor = kwargs.get("d_important_word_factor", 0.75),
        )
    
    def process_survey(self, survey_file, non_industry_score_factor=0.75, **kwargs):
        '''
            Load in a survey file with company names and try to find the best matches for each of them
            to a company in the master db
            Arguments:
                survey_file: {str} path to excel file with one or more survey sheets
                non_industry_score_factor: {float} multiply match scores by this if they map to
                   companies that aren't in the specified industry/sic code.
                   A high percentage will act like a tie-breaker for company names/acronyms that 
                   cross industries, while a low percentage will make the matcher choose a worse
                   company match from the same industry. -1 will remove non-industry/sic matches entirely.
                                           
        '''
        
        e_survey_companies = read_write.load_survey(survey_file, **kwargs)
        
        e_survey_matches = {}
        n_start = time()
        print(f"\n{len(e_survey_matches)} Survey sheet(s) Loaded. Starting matching of names.\n")
        for i_survey, (s_survey, c_companies) in enumerate(e_survey_companies.items()):
            n_survey_start = time()
            
            e_survey_row_matches = {}
            
            print(f'Finding matches for survey {i_survey}: "{s_survey}" with {len(c_companies):,} companies.')
            for i_company, s_company in enumerate(c_companies):
                if i_company % 100 == 0 and i_company > 0:
                    print(f"\tsearching: {i_company:,} ({(i_company + 1) / len(c_companies):0.2%}). Elapsed: {secondsToStr(time() - n_survey_start)}", )
                
                e_survey_row_matches[s_company] = self.get_matches(s_company, d_non_industry_score_factor=non_industry_score_factor)
            
            e_survey_matches[s_survey] = e_survey_row_matches
            print(f"\tSurvey {i_survey} took: {secondsToStr(time() - n_survey_start)}. Total elapsed: {secondsToStr(time() - n_start)}")

        print(f"\nElapsed: {secondsToStr(time() - n_start)}\n")
        print(f"Creating output sheet of survey company matches")
        df_output = self.output_results(e_survey_matches, **kwargs)
        
        return e_survey_matches, df_output
    
    def output_results(self, e_survey_matches, s_output_file="", **kwargs):
        ''' Turn the output matches into a dataframe for export to excel
            Arguments:
                e_survey_matches: {dict} of dicts of matches for each survey
                    e.g. {"survey_name_1": {"company 1": [matches_for_company_1], "company 2": [matches_for_company_2], ...}}
                s_output_file: {str} optional file name to write the output directly to
            Returns:
                {pd.DataFrame} of the output table of all companies found in any sheet of the input and their matches
        '''
        i_output_matches = kwargs.get("i_output_matches", i_OUTPUT_MATCHES)

        e_survey_outputs = defaultdict(list)
        a_all_outputs = []
        for i_survey, (s_survey, e_matches) in enumerate(e_survey_matches.items()):
            #if s_survey != "Dietrich - Architecture": continue
            #print(s_survey, len(e_matches))
            
            for s_company, a_matches in e_matches.items():
                i_version_matches = any(e.get("kind") == "version_match" for e in a_matches)
                b_version_match = any(e.get("kind") == "version_match" for e in a_matches)
                e_row = {
                    "Group": i_survey + 1,
                    "Survey": s_survey,
                    "Company": s_company,}
                for i_match, e_match in enumerate(a_matches[:i_output_matches]):
                    e_row[f"Matched Company {i_match + 1}"] = e_match["mapped_company"]
                    e_row[f"Confirm {i_match + 1}"] = 1 if e_match.get("kind") == "confirmed_match" else None
                    e_row[f"Score {i_match + 1}"] = e_match["score"]
                    e_row[f"Kind {i_match + 1}"] = e_match["kind"]
                    e_row[f"Version {i_match + 1}"] = e_match["search_type"]
                    #e_row[f"Match {i_match + 1}"] = e_match["text"]
                    e_row[f"Search Version {i_match + 1}"] = e_match["search"]
                    e_row[f"Match Version {i_match + 1}"] = e_match["text"]
                    #e_row[f"Match Original Name(s) {i_match + 1}"] = "|".join(e_match["mapped_company"])
                
                e_survey_outputs[s_survey].append(e_row)
                a_all_outputs.append(e_row)
                #break
        df_output = pd.DataFrame(a_all_outputs)
        
        a_master_cols_to_add = kwargs.get("master_cols_to_add", ["Industry", "SIC Code"])
        if a_master_cols_to_add:
            e_cols_to_add = self._master_db.set_index(s_DB_COMPANY_COL)[a_master_cols_to_add].to_dict(orient="Index")
            for i in range(1, i_output_matches + 1):
                s_match_company_col = f"Matched Company {i}"
                if s_match_company_col not in df_output.columns: continue
                i_col = list(df_output.columns).index(s_match_company_col)
                for i_add, s_col in enumerate(a_master_cols_to_add):
                    df_output.insert(
                        i_col + i_add + 1,
                        f"{s_col} {i}",
                        df_output[s_match_company_col].apply(lambda x: e_cols_to_add.get(x, {}).get(s_col)))
        
        if kwargs.get("b_sort"):
            df_output.sort_values(["Group", "Kind 1", "Score 1"], ascending=[True, False, False], inplace=True)
        
        if s_output_file:
            if not s_output_file.lower().endswith((".xlsx", ".xlsm")):
                s_output_file += ".xlsx"
            o_writer = pd.ExcelWriter(s_output_file)
            df_output.to_excel(o_writer, index=False)
            o_writer.save()
            o_writer.close()
        
        return df_output
    
    def get_report(self, df_output, **kwargs):
        ''' Generate a report sheet on the scores of the matches, and concentrations of industry/sic codes
            Arguments:
                df_output: {pd.DataFrame} from self.output_results() with added columns of industry and sic codes for each match tier
            Returns:
                {pd.DataFrame} of the report on each survey sheet
        '''
        print(f"Creating report on accuracy of matching and Industry/SIC concentrations")
        df_scores = None
        if f"Score 1" in df_output.columns:
            e_survey_scores = defaultdict(dict)
            e_survey_scores[(0, "overall")] = {"Average Score": df_output[f"Score 1"].fillna(0).mean()}
            
            for (i_group, s_survey), df_survey in df_output.groupby(["Group", "Survey"]):
                e_survey_scores[(i_group, s_survey)] = {"Average Score": df_survey[f"Score 1"].fillna(0).mean()}
            df_scores = pd.DataFrame.from_dict(e_survey_scores, orient="index")
    
        e_value_percents, e_value_concentration = self.analyze_concentrations(df_output, **kwargs)
        df_concentrations = pd.DataFrame.from_dict(e_value_concentration, orient="index")
        i_most_common = kwargs.get("i_most_common", i_MOST_COMMON)
        a_concentration_cols = list(df_concentrations.columns)
        
        for s_col in a_concentration_cols:
            i_col = list(df_concentrations.columns).index(s_col)
            a_top_vals = [
                e_value_percents.get(t_survey, {}).get(s_col, [])[:i_most_common]
                for t_survey in df_concentrations.index]
            
            for i_top in range(0, i_most_common):
                a_vals, a_percents = zip(*[a_top[i_top] if len(a_top) > i_top else (None, None) for a_top in a_top_vals])
                df_concentrations.insert(
                    i_col + (i_top * 2) + 1,
                    f"{s_col} - {ordinal(i_top + 1)} Highest",
                    a_vals
                )
                df_concentrations.insert(
                    i_col + (i_top * 2) + 2,
                    f"{s_col} - {ordinal(i_top + 1)} Highest %",
                    a_percents
                )
        df_concentrations.rename(columns={col: f"{col} Herfindahl" for col in a_concentration_cols}, inplace=True)
        
        if df_scores is None:
            df_report = df_concentrations
        else:
            df_report = df_scores.join(df_concentrations)
        
        df_report = df_report.reset_index().sort_values("level_0", ascending=True).drop(columns=["level_0"])
        df_report.rename(columns={"level_0": "Group", "level_1": "Survey"}, errors="ignore", inplace=True)
        return df_report
    
    def analyze_concentrations(self, df_output, **kwargs):
        ''' Analyze the concentrations of industries and sic codes using the frequencies found with the matches
            on the master company db, weighted by the relative confidence of the matches. And the most frequent
            industries/sic codes for each survey sheet.
            Arguments:
                df_output: {pd.DataFrame} from self.output_results() with added columns of industry and sic codes for each match tier
            Returns:
                {tuple} of two {dicts} e_value_percents, e_value_concentration
        '''
        #e_value_scores = defaultdict(Counter)
        #e_value_concentration = {}
        e_value_scores = defaultdict(lambda: defaultdict(Counter))
        e_value_concentration = defaultdict(dict)
    
        i_output_matches = kwargs.get("i_output_matches", i_OUTPUT_MATCHES)
    
        a_master_cols_to_add = kwargs.get("master_cols_to_add", ["Industry", "SIC Code"])
        if a_master_cols_to_add:
            for s_col in a_master_cols_to_add:
                for i in range(1, i_output_matches + 1):
                    s_score_col = f"Score {i}"
                    s_add_col = f"{s_col} {i}"
                    if s_add_col not in df_output.columns or s_score_col not in df_output.columns: continue
        
                    #print(s_score_col, s_add_col)
                    df_col = df_output[[s_add_col, s_score_col]].copy().dropna(how="any")
                    e_value_scores[(0, "overall")][s_col].update(dict(zip(df_col[s_add_col], df_col[s_score_col])))
                e_value_concentration[(0, "overall")][s_col] = herfindahl_freqs_dict(e_value_scores[(0, "overall")][s_col])
        
        
        
            for (i_group, s_survey), df_survey in df_output.groupby(["Group", "Survey"]):
                for s_col in a_master_cols_to_add:
        
                    for i in range(1, i_output_matches + 1):
                        s_score_col = f"Score {i}"
                        s_add_col = f"{s_col} {i}"
                        if s_add_col not in df_survey.columns or s_score_col not in df_survey.columns: continue
        
                        #print(s_survey, s_score_col, s_add_col)
                        df_col = df_survey[[s_add_col, s_score_col]].copy().dropna(how="any")
                        e_value_scores[(i_group, s_survey)][s_col].update(dict(zip(df_col[s_add_col], df_col[s_score_col])))
        
                    e_value_concentration[(i_group, s_survey)][s_col] = herfindahl_freqs_dict(e_value_scores[(i_group, s_survey)][s_col])
        
        e_value_percents = {
            t_survey: {
                s_col: sorted(freq_dict_to_percent(e_vals).items(), key=itemgetter(1), reverse=True)
                for s_col, e_vals in e_cols.items()}
            for t_survey, e_cols in e_value_scores.items()
        }
    
        return e_value_percents, e_value_concentration
    
    ##### FILTER MASTER DB #####
    def get_industry_companies(self, df_master=None, a_industries=[], a_sic_codes=[], **kwargs):
        ''' Get the list of companies to search against by filtering with industries or SIC Codes
            Arguments:
                df_master: {pd.DataFrame} of the master company list
                a_industries: {list} of values that a company must have in at
                              least one of the industry columns to be included in the output
                a_sic_codes: {list} of values that a company must have in
                             the SIC Code column to be included in the output
            Returns:
                {pd.DataFrame} of the companies that match the specified industries/SIC codes
        '''
        if not isinstance(df_master, pd.DataFrame):
            df_industry = self._master_db.copy()
        else:
            df_industry = df_master.copy()
        if a_industries:
            df_industry = df_industry[
                (df_industry["Industry"].isin(a_industries)) |
                (df_industry["Primary Industry (Zoom)"].isin(a_industries)) |
                (df_industry["Sub Industry (Zoom)"].isin(a_industries))]
        
        if a_sic_codes:
            df_industry = df_industry[(df_industry["SIC Code"].isin(a_sic_codes))]
        
        print(f"Filter down to {len(df_industry):,} companies (from {len(df_master):,} companies in original list)")
        return df_industry

    ##### GETTING NAME VERSIONS OF A COMPANY #####
    def clean_company_words(self, text, b_remove_all=False, **kwargs):
        ''' Remove or substitute words from the company text by splitting into
            words and then checking if each is in the list above
            Arguments:
                text: {str} of the full company text
            Returns:
                {str} of text with words abbreviated/removed/translated as specified
        '''
        for s_val, re_phrase in e_COMPANY_REGEXES.items():
            text = re_phrase.sub(s_val.upper(), text)
        
        if b_remove_all:
            if isinstance(text, str):
                text = text.split()
            new_text = [
                word for word in [
                    word.strip() for word in text
                    if not re.sub("[-+.,'\"()\[\]]", "", word.lower()) in c_DELS
                    ]
                if word]
            return ' '.join(new_text).strip()
        else:
            if isinstance(text, str):
                text = text.split()
            new_text = [
                word.strip() for word in [
                    e_SUBS.get(re.sub("[-+.,'\"()\[\]]", "", word.lower()), word)
                    for word in text]
                if word]
            return ' '.join(new_text).strip()
    
    def make_company_acronym(
        self,
        s_text,
        i_min_name_len=6,
        b_split_camelcase=True,
        b_split_alphanum=True,
        b_keep_numbers=True,
        b_keep_non_dict=True,
        i_min_len=3,
        b_skip_company_types=True,
        **kwargs):
        ''' Try to make an acronym from a company name as an additional fuzzy match alternative for long company names
            Arguments:
                i_min_name_len: {int}  # the minimum number of characters in the original name to even try creating an acronym
                b_split_camelcase: {bool}  # split CamelCase words first so that they can become separate letters in the acronym (e.g. "CC" instead of "C")
                b_split_alphanum: {bool}  # split adjacent letters and numbers so that neither is completely excluded from the acronym (e.g. "Rapid7" becomes "R7" instead of "R")
                b_keep_numbers: {bool}  # keep the whole number so that it isn't truncated (e.g. "Area 51" would become "A51" instead of "A5")
                b_keep_non_dict=True,  # keep whole words that are non-dictionary as long as they are short. Trying to capture existing acronyms/abbreviations already in the company name.
                i_min_len=3,  # the minimum number of characters in the abbreviation. Acronyms shorter than this will be dropped
            Returns:
                {str} of the acronym if one can be created, otherwise {None}
        '''
        b_is_upper = s_text.isupper()
        if len(s_text) < i_min_name_len: return None
        
        if b_split_camelcase:
            s_text = re.sub(r"(?<=[a-z]{3,3})(?=[A-Z])", " ", s_text)
        if b_split_alphanum:
            s_text = re.sub(r"((?<=[a-z])(?=\d)|(?<=\d)(?=[a-z]))", " ", s_text)
        s_text = re.sub(r"\band\b", " & ", s_text, flags=re.I)
        
        a_words = [x for x in re.split(f"[-_ +/\\.,\[\]]+", s_text) if x]
        
        a_acronym = []
        for s_word in a_words:
            s_lower = s_word.lower()
            b_word_is_upper = s_word.isupper()
            b_is_skip_company_type = s_word.lower() in c_SKIP_COMPANY_ABBREVIATIONS 
            b_is_company_type = b_is_skip_company_type or s_word.lower() in c_COMPANY_ABBREVIATIONS 
            b_unabbreviated_word = False
            
            if b_is_skip_company_type or ((b_skip_company_types or b_unabbreviated_word) and b_is_company_type):
                continue
            elif s_lower in c_SKIP_ACRONYM_WORDS:
                continue
            elif s_lower in c_COMPANY_GROUP_ABBRS:
                a_acronym.append(s_word[0])
            elif b_keep_numbers and re.search(f"^\d$", s_word):
                a_acronym.append(s_word)
            elif b_keep_non_dict and s_lower not in self._common_english and not b_is_company_type and len(s_word) < 5:
                a_acronym.append(s_word)
                b_unabbreviated_word = True
            elif not b_is_upper and b_word_is_upper and not b_is_company_type and len(s_word) < 5:
                a_acronym.append(s_word)
                b_unabbreviated_word = True
            else:
                a_acronym.append(s_word[0])
        s_acronym = "".join(a_acronym).upper()
        if len(s_acronym) < i_min_len: return None
        return s_acronym
    
    def get_name_versions(self, s_company, **kwargs):
        ''' Create the unique versions of the company name with standardized punctuation, abbr/removed common company words, acronym.
            Will only add new version if it doesn't already existing (i.e. if "original" already has abbreviated "inc",
            then "abbr_company_words" won't be added to output dict).
            Arguments:
                s_company: {str} of company in the master db or company name from survey
            Returns:
                {dict} of {version_type: "company name version"}
        '''
        c_invalids = self._invalids.get(s_company.lower(), [])
        if kwargs.get("b_debug"): print(s_company, "c_invalids", c_invalids)
        
        s_clean = re.sub("(?<=[a-z])[.](?=[a-z])", "", s_company, flags=re.I)
        s_clean = re.sub("(^the\b|, *the$)", "", s_clean, flags=re.I)
        s_clean = re.sub(" *[-_.,\[\]\\/\(\)'\"?]+ *", " ", s_clean, flags=re.I).strip()
        e_versions = {
            "original": s_company,
        }
        c_versions = {s_company.lower()}
        
        s_clean_lower = s_clean.lower()
        if s_clean_lower and s_clean_lower not in c_versions and s_clean_lower not in e_versions.values() and s_clean_lower not in c_invalids:
            e_versions["clean"] = s_clean_lower
            c_versions.add(s_clean_lower)
        
        s_abbr = self.clean_company_words(s_clean, **kwargs)
        if s_abbr and s_abbr.lower() not in c_versions and s_abbr.lower() not in e_versions.values() and s_abbr.lower() not in c_invalids:
            e_versions["abbr_company_words"] = s_abbr.lower()
            c_versions.add(s_abbr.lower())
        
        s_rem = self.clean_company_words(s_clean, b_remove_all=True, **kwargs)
        if s_rem and s_rem.lower() not in c_versions and s_rem.lower() not in e_versions.values() and s_rem.lower() not in c_invalids:
            e_versions["removed_company_words"] = s_rem.lower()
            c_versions.add(s_rem.lower())
        
        s_acro = self.make_company_acronym(s_clean, **kwargs)
        if s_acro and s_acro.lower() not in c_versions and s_acro.lower() not in e_versions.values() and s_acro.lower() not in c_invalids:
            e_versions["acronym"] = s_acro.lower()
            c_versions.add(s_acro.lower())
        return e_versions
    
    def get_company_name_versions(self, df_companies, **kwargs):
        ''' Assemble the different version of each companies name from master db to go into the fuzzy matcher
            Arguments:
                df_companies: {pd.DataFrame} of the master company list db
        '''
        e_companies = defaultdict(list)
        #e_kinds = defaultdict(lambda: defaultdict(list))
        e_mapping_kinds = defaultdict(set)
        
        b_original = kwargs.get("original", True)
        b_clean = kwargs.get("clean", True)
        b_abbr_common = kwargs.get("abbr_common", True)
        b_remove_common = kwargs.get("remove_common", True)
        b_make_acronyms = kwargs.get("make_acronyms", True)
        
        for s_company in df_companies[s_DB_COMPANY_COL]:
            
            e_versions = self.get_name_versions(s_company, **kwargs)
            
            if b_original and e_versions.get("original"):
                e_companies[s_company].append(e_versions["original"].lower())
                e_mapping_kinds[e_versions["original"].lower()].add("original")
            if b_clean and e_versions.get("clean"):
                e_companies[s_company].append(e_versions["clean"])
                e_mapping_kinds[e_versions["clean"]].add("clean")
            if b_abbr_common and e_versions.get("abbr_company_words"):
                e_companies[s_company].append(e_versions["abbr_company_words"])
                e_mapping_kinds[e_versions["abbr_company_words"]].add("abbr_company_words")
            if b_remove_common and e_versions.get("removed_company_words"):
                e_companies[s_company].append(e_versions["removed_company_words"])
                e_mapping_kinds[e_versions["removed_company_words"]].add("removed_company_words")
            if b_make_acronyms and e_versions.get("acronym"):
                e_companies[s_company].append(e_versions["acronym"])
                e_mapping_kinds[e_versions["acronym"]].add("acronym")
        
        e_mappings = defaultdict(list)
        for s_company, a_alts in e_companies.items():
            for s_alt in a_alts:
                e_mappings[s_alt].append(s_company)
        
        return e_companies, e_mappings, e_mapping_kinds
    
    
    def disambiguate_kinds(self, e_mapping_kinds):
        ''' pick the least valuable kind for e_mapping_kinds of a single version might be several of them '''
        return {
            s_version:
            "acronym" if "acronym" in c_types else
            "removed_company_words" if "removed_company_words" in c_types else
            "abbr_company_words" if "abbr_company_words" in c_types else
            "clean" if "clean" in c_types else
            "original"
            for s_version, c_types in e_mapping_kinds.items()
        }
    
    ##### GETTING BEST MATCHES FOR A COMPANY #####
    def get_matches(self, s_company, b_dedupe=True, **kwargs):
        ''' Find the matches for a single company.
            Look first for confirmed matches,
            then exact matches against one of the version of the input company,
            then last use fuzzy matching for any version of the company
            Arguments:
                s_company: {str} of the company name to search for
            Returns:
                {list} of potential matches {dict} sorted from best matches to worst
        '''
        e_versions = self.get_name_versions(s_company, **kwargs)
        
        # Look for confirmed matches
        a_matches = self.get_confirmed_matches(e_versions, b_sort=False, **kwargs)
        
        # Look for matches based on cleaned/abbreviated/acronym versions and filter out disproved matches
        a_version_matches = self.get_version_matches(e_versions, b_sort=False, **kwargs)
        a_matches.extend(self.filter_disproved_matches(a_version_matches, e_versions, **kwargs))
        
        # Get fuzzy matches and Filter out disproved matches
        if not a_matches or not any(e_match["search_type"] != "acronym" and e_match["score"] > 0.5 for e_match in a_matches):
            a_fuzzy_matches = self.get_fuzzy_matches(e_versions, b_sort=False, **kwargs)
            a_matches.extend(
                self.filter_disproved_matches(a_fuzzy_matches, e_versions, b_sort=False, **kwargs))
        
        # filter out or reduce the score of matches that aren't in specified industries/sic codes
        a_matches = self.filter_metadata_matches(a_matches, **kwargs)
        
        # sort in descending order of best matches to worst
        a_matches.sort(key=lambda e_match: e_match["score"], reverse=True)
        
        # remove duplicate matches that point to the same company, keeping the best scoring version
        # since it happens after sorting
        if b_dedupe:
            a_matches = self.dedupe_matches(a_matches)
        
        return a_matches
    
    
    def get_confirmed_matches(self, e_versions, b_sort=True, **kwargs):
        ''' Find any matches that were already confirmed as good in prior runs and read
            in from the output or from saved validation file
            Arguments:
                e_version: {dict} of {version_type: "company name version"} of the specific company being searched for
                b_sort: {bool} sort matches from best to worst in output
            Returns:
                {list} of matches {dict} sorted from best matches to worst
        '''
        a_all_matches = []
        for s_type, s_version in e_versions.items():
            c_valid = self._validations.get(s_version.lower(), {}).get("valid", set())
            
            for s_company in c_valid:
                a_all_matches.append({
                    #'idx': -1,
                    "kind": "confirmed_match",
                    'text': s_version,
                    #'total_ngrams': 1,
                    #'matched_ngrams': 1,
                    'just_search_ngrams': 0,
                    'just_candidate_ngrams': 0,
                    #'matched_score': 0.0,
                    'unmatched_score': 0.0,
                    'ngram_score': 1.0,
                    'score': e_VERSION_SCORES.get("confirmed", 1.0),
                    'search_type': s_type,
                    'search': s_version,
                    "mapped_company": s_company,
                })
        
        if b_sort:      
            #a_all_matches.sort(key=lambda e_match: -e_match['score'] ** e_TYPE_EXP[e_match['search_type']] * e_TYPE_FACTORS[e_match['search_type']])
            a_all_matches.sort(key=lambda e_match: e_match['score'], reverse=True)
        
        #c_unique_mappings = set()
        #a_unique_matches = []
        #for e_match in a_all_matches:
        #    if e_match["mapped_company"] in c_unique_mappings: continue
        #    a_unique_matches.append(e_match)
        #    c_unique_mappings.add(e_match["mapped_company"])
        
        return a_all_matches
    
    def get_version_matches(self, e_versions, b_sort=True, **kwargs):
        ''' Find any exact matches between the versions generated from the
            searched company and versions of companys in the master db
            Arguments:
                e_version: {dict} of {version_type: "company name version"} of the specific company being searched for
                b_sort: {bool} sort matches from best to worst in output
            Returns:
                {list} of potential matches {dict} sorted from best matches to worst
        '''
        a_all_matches = []
        for s_type, s_version in e_versions.items():
            a_mapped = self._mappings.get(s_version.lower(), [])
            for s_company in a_mapped:
                a_all_matches.append({
                    #'idx': -1,
                    "kind": "version_match",
                    'text': s_version,
                    #'total_ngrams': 1,
                    #'matched_ngrams': 1,
                    'just_search_ngrams': 0,
                    'just_candidate_ngrams': 0,
                    #'matched_score': 0.0,
                    'unmatched_score': 0.0,
                    'ngram_score': 1.0,
                    'score': e_VERSION_SCORES.get(s_type, 1.0) if s_type == "original" else e_VERSION_SCORES.get(s_type, 0.5) * min(1.0, len(s_version) / min(10, len(s_company))),
                    'search_type': s_type,
                    'search': s_version,
                    "mapped_company": s_company,
                })
                
        if b_sort:
            #a_all_matches.sort(key=lambda e_match: -e_match['score'] ** e_TYPE_EXP[e_match['search_type']] * e_TYPE_FACTORS[e_match['search_type']])
            a_all_matches.sort(key=lambda e_match: e_match['score'], reverse=True)
        
        #c_unique_mappings = set()
        #a_unique_matches = []
        #for e_match in a_all_matches:
        #    if e_match["mapped_company"] in c_unique_mappings: continue
        #    a_unique_matches.append(e_match)
        #    c_unique_mappings.add(e_match["mapped_company"])
        
        #return [e_match for a_matches in e_unique_matches.values() for e_match in a_matches]
        return a_all_matches
    
    
    def get_fuzzy_matches(self, e_versions, b_sort=True, **kwargs):
        ''' Find any fuzzy matches between the search company and companies in the master db
            Arguments:
                e_version: {dict} of {version_type: "company name version"} of the specific company being searched for
                b_sort: {bool} sort matches from best to worst in output
            Returns:
                {list} of potential matches {dict}, optionally sorted from best matches to worst
        '''
        a_alt1_matches, a_alt2_matches, a_acronym_matches = [], [], []
        a_orig_matches, a_clean_matches = [], []
        
        #a_orig_matches = o_fuzzy_companies.search(s_company)
        if e_versions.get("abbr_company_words"):
            a_alt1_matches = self._fuzzy_matcher.search(e_versions["abbr_company_words"], d_min_ngram_score=kwargs.get("d_min_ngram_score", 0.2))
        
        if e_versions.get("removed_company_words"):
            a_alt2_matches = self._fuzzy_matcher.search(e_versions["removed_company_words"])
        
        if not a_alt1_matches and not a_alt2_matches:
            if e_versions.get("clean"):
                a_clean_matches = self._fuzzy_matcher.search(e_versions["clean"])
                if not a_clean_matches:
                    a_orig_matches = self._fuzzy_matcher.search(e_versions["original"])
            else:
                a_orig_matches = self._fuzzy_matcher.search(e_versions["original"])
        
        if e_versions.get("acronym"):
            a_acronym_matches = self._fuzzy_matcher.search(e_versions["acronym"])
        
        a_type_matches = []
        if a_alt1_matches: a_type_matches.append((e_versions["abbr_company_words"], "abbr_company_words", a_alt1_matches))
        if a_alt2_matches: a_type_matches.append((e_versions["removed_company_words"], "removed_company_words", a_alt2_matches))
        if a_acronym_matches: a_type_matches.append((e_versions["acronym"], "acronym", a_acronym_matches))
        if a_clean_matches: a_type_matches.append((e_versions["clean"], "clean", a_clean_matches))
        if a_orig_matches: a_type_matches.append((e_versions["original"], "original", a_orig_matches))
        
        
        e_all_matches = {}
        for s_version, s_type, a_matches in a_type_matches:
            s_best_match = f'Best match: {a_matches[0]["text"]} ({a_matches[0]["score"]:0.2%}).' if a_matches else "No matches."
            if kwargs.get("b_debug"):
                print(f'{s_type}: {len(a_matches)} matches with "{s_version}". {s_best_match}')
            for e_match in a_matches:
                e_match["kind"] = "fuzzy_match"
                e_match["search_type"] = s_type
                e_match["search"] = s_version
                e_match["mapped_company"] = " | ".join(self._mappings.get(e_match["text"]))
                
                #print(e_match["text"])
                #print(self._mapping_kinds[e_match["text"]])
                if e_VERSION_SCORES[s_type] <= e_VERSION_SCORES[self._mapping_kinds[e_match["text"]]]:
                    e_match["score"] = e_match["score"] ** e_TYPE_EXP[s_type] * e_TYPE_FACTORS[s_type]
                else:
                    e_match["score"] = e_match["score"] ** e_TYPE_EXP[self._mapping_kinds[e_match["text"]]] * e_TYPE_FACTORS[self._mapping_kinds[e_match["text"]]]
                
                s_match, d_score = e_match["text"], e_match["score"]
                
                if s_match not in e_all_matches:
                    e_all_matches[s_match] = e_match
                else:
                    #if d_score > e_all_matches[s_match]["score"] ** e_TYPE_EXP[e_all_matches[s_match]["search_type"]] * e_TYPE_FACTORS[e_all_matches[s_match]["search_type"]]:
                    if d_score > e_all_matches[s_match]["score"]:
                        e_all_matches[s_match] = e_match
        
        a_all_matches = list(e_all_matches.values())
        if b_sort:
            a_all_matches.sort(key=lambda e_match: e_match['score'], reverse=True)
        
        #c_unique_mappings = set()
        #a_unique_matches = []
        #for e_match in a_all_matches[:i_max_matches]:
        #    if e_match["mapped_company"] in c_unique_mappings: continue
        #    a_unique_matches.append(e_match)
        #    c_unique_mappings.add(e_match["mapped_company"])
        
        return a_all_matches
    
    def dedupe_matches(self, a_matches, **kwargs):
        ''' Deduplicate matches pointing to the same mapped company
            Arguments:
                a_matches: {list} of potential matches {dict} from get_version_matches or get_fuzzy_matches, sorted from best matches to worst
            Returns:
                {list} of unique matches where
        '''
        a_matches.sort(key=lambda e_match: e_match['score'], reverse=True)
                
        c_unique_mappings = set()
        a_unique_matches = []
        for e_match in a_matches:
            if e_match["mapped_company"] in c_unique_mappings: continue
            a_unique_matches.append(e_match)
            c_unique_mappings.add(e_match["mapped_company"])
        
        return a_unique_matches
    
    def filter_disproved_matches(self, a_matches, e_versions, b_keep_suspicious=True, **kwargs):
        ''' Filter out any matches from a list where the mapped company has already been marked as invalid by a human
            Arguments:
                a_matches: {list} of potential matches {dict} from get_version_matches or get_fuzzy_matches, sorted from best matches to worst
                e_version: {dict} of {version_type: "company name version"} of the specific company being searched for
                b_keep_suspicious: {bool} to whether keep matches where the suggestion has many invalid matches and just reduce the score
            Returns:
                {list} of filtered matches {dict} sorted from best matches to worst
        '''
        e_versions
        a_filtered_matches = []
        c_invalid = set()
        for s_type, s_version in e_versions.items():
            c_invalid.update(self._validations.get(s_version.lower(), {}).get("invalid", set()))
        #c_invalid = self._validations.get(e_versions["original"].lower(), {}).get("invalid", set())
    
        for e_match in a_matches:
            if e_match["text"] in c_invalid or e_match["mapped_company"].lower() in c_invalid: continue
            
            if None in c_invalid or "" in c_invalid:
                if b_keep_suspicious:
                    e_match["score"] *= 0.1
                else:
                    continue
            
            a_filtered_matches.append(e_match)
        
        if b_keep_suspicious:
            a_filtered_matches.sort(key=lambda e_match: e_match['score'], reverse=True)
        
        return a_filtered_matches
    
    def filter_metadata_matches(self, a_matches, c_industries=None, c_sic_codes=None, d_non_industry_score_factor=-1, b_sort=True, **kwargs):
        ''' Filter out or reduce the score of matches where the mapped company is not in the specified industries or sic codes
            Arguments:
                a_matches: {list} of potential matches {dict} from get_version_matches or get_fuzzy_matches, sorted from best matches to worst
                c_industries: {set} of industries to match on (will match values in "Industry", "Primary Industry (Zoom)", or "Sub Industry (Zoom)")
                c_sic_codes: {set} of sic_codes to match on (will match value in "SIC Code")
                d_non_industry_score_factor: {float} to reduce the score rather than deleting the match entirely
            Returns:
                {list} of filtered matches {dict} sorted from best matches to worst
        '''
        if c_industries is None:
            c_industries = self._industries
        elif not isinstance(c_industries, set):
            c_industries = set(c_industries)
        if c_sic_codes is None:
            c_sic_codes = self._sic_codes
        elif not isinstance(c_sic_codes, set):
            c_sic_codes = set(c_sic_codes)
        
        if not c_industries and not c_sic_codes: return a_matches
        
        if kwargs.get("b_debug"):
            print("c_industries", c_industries)
            print("c_sic_codes", c_sic_codes)
        
        if d_non_industry_score_factor > 0:
            for e_match in a_matches:
                s_company = e_match["mapped_company"]
                e_metadata = self._company_metadata.get(s_company, {})
                
                if kwargs.get("b_debug"): print("e_metadata", e_metadata)
                b_matched = False
                
                if c_sic_codes:
                    if e_metadata.get(s_DB_SIC_CODE_COL) in c_sic_codes:
                        b_matched = True
                
                if c_industries:
                    if c_industries.intersection({e_metadata.get(x) for x in a_INDUSTRY_COLS}):
                        b_matched = True
                
                if not b_matched:
                    e_match["score"] *= d_non_industry_score_factor
                    
            a_filtered_matches = sorted(a_matches, key=lambda e_match: e_match['score'], reverse=True)
        else:
            a_filtered_matches = []
            for e_match in a_matches:
                s_company = e_match["mapped_company"]
                e_metadata = self._company_metadata.get(s_company, {})
                
                if kwargs.get("b_debug"): print("e_metadata", e_metadata)
                b_matched = False
                
                if c_sic_codes:
                    if e_metadata.get(s_DB_SIC_CODE_COL) in c_sic_codes:
                        b_matched = True
                
                if c_industries:
                    if c_industries.intersection({e_metadata.get(x) for x in a_INDUSTRY_COLS}):
                        b_matched = True
                
                if b_matched:
                    a_filtered_matches.append(e_match)
        
        if b_sort and d_non_industry_score_factor > 0:
            a_filtered_matches.sort(key=lambda e_match: e_match['score'], reverse=True)
        
        return a_filtered_matches

##### End CompanyMatcher Class


##### Other utilities #####

def secondsToStr(t):
    ''' convert seconds into a human-readable time string '''
    return "%d:%02d:%02d.%03d" % \
        reduce(lambda ll,b : divmod(ll[0],b) + ll[1:],
            [(t*1000,),1000,60,60])


def herfindahl_freqs_dict(e_freq, b_ignoreblanks=False):
    ''' Calculate the HHI from a frequency dictionary'''
    if b_ignoreblanks == True:
        if '' in e_freq: del e_freq['']
        if '0' in e_freq: del e_freq['0']
        if 'nan' in e_freq: del e_freq['nan']
        if 0 in e_freq: del e_freq[0]
        if np.nan in e_freq: del e_freq[np.nan]
        
    d_total = sum(e_freq.values())
    if d_total != 0:
        return sum([(x / d_total)**2 for x in e_freq.values()])
    return 0


def freq_dict_to_percent(e_freq, i_total=0):
    ''' Convert a frequency dictionary containing counts into percents.
        If i_total is not passed it, it will be calculated by summing individual counts. '''
    if not i_total:
        i_total = sum(e_freq.values())
        
    if i_total:
        return {v_key: float(i_count) / i_total for v_key, i_count in e_freq.items()}
    else:
        return e_freq


def ordinal(n):
    ''' Convert an integer into its respect ordinal. 1 -> "1st", 2 -> "2nd", etc '''
    return "%d%s" % (n,"tsnrhtdd"[(n//10%10!=1)*(n%10<4)*n%10::4])