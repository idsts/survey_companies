# survey_companies

This is a project meant to help match up a list of company names to a master list of companies using versions of the company name, plus fuzzy text matching.


# Installation

```
pip install git+https://bitbucket.org/idsts/survey_companies
```

# Tests

Set up for testing, by installing the dev and dist dependencies:
```
pip install -r requirements.dev.txt
pip install -r requirements.dist.txt
```

## Running naive matches:
```
#!python
import pandas as pd
from survey_companies import company_prep, read_write

# Load the master company db and prep the fuzzy library
o_Matcher = company_prep.CompanyMatcher(
    # path to excel file with master db sheet
    "Master List.xls",
)
o_Matcher.create_fuzzy_matcher()

# load the batch of survey(s) and match the companies found in them
e_survey_matches, df_output = o_Matcher.process_survey(
    r"Participant list (1).xlsx",
    sheet_names=[  # exclude sheet_names to load all sheets in the file
        "McLagan Fin Tech",
        "Dietrich - Architecture",
        "Salary - IPAS Global Benchmark",
        "WMG Credit Unions",
        "PM - Cyber Security",
    ],
    non_industry_score_factor=0.75, # multiply match scores by this if they aren't part of the specified industries. No effect if no industries/sic codes specified
)

# generate a report on the accuracy of matching and herfindahl concentration of industry/sic code
df_report = o_Matcher.get_report(df_output)

# write the results and report to a workbook
o_writer = pd.ExcelWriter("output.xlsx")
df_output.to_excel(o_writer, sheet_name="Survey Companies", index=False)
df_report.to_excel(o_writer, sheet_name="Report", index=False)
o_writer.save()
o_writer.close()


# save the initial mappings to a validations file
df_validated = read_write.store_validations(
    e_validations=o_Matcher._validations,
    e_mapping_kinds=o_Matcher._mapping_kinds,
    e_mappings=o_Matcher._mappings,  #use None to not save unverified versions generated from master company DB
    df_validations=None,
    s_output="validated_company_matches.xlsx")
print(f"{len(df_validated):,} saved validations")
```


## Learning from user feedback:
```
#!python
e_validations = None
e_mappings = None
e_mapping_kinds = None
e_valids = None
e_invalids = None

# read the "Account Name", "Alternate", "Confirm", "Version" columns of previously saved validations
e_validations, e_mappings, e_mapping_kinds, e_valids, e_invalids = read_write.load_validations(
    # path to saved validation file with "Account Name", "Alternate", "Confirm", "Version" columns
    r"validated_company_matches.xlsx")
print(len(e_validations), len(e_mapping_kinds))
print(len(e_valids), len(e_invalids))

# add to the previously saved validations by reading the "Confirm1", "Confirm2", "Confirm3" columns to save matches that a user has marked as definitely good with +1 or definitely bad with -1
e_validations, e_valids, e_invalids = read_write.read_output_validations(
    r"output.xlsx",
    e_validations=e_validations,
    e_valids=e_valids,
    e_invalids=e_invalids)

# Save the learned valiations to a worksheet for the next run
df_validated = read_write.store_validations(
    e_validations=e_validations,
    e_mapping_kinds=e_mapping_kinds,
    e_mappings=o_Matcher._mappings,  #use None to not save unverified versions generated from master company DB
    df_validations=None,
    s_output="validated_company_matches_updated.xlsx")
print(f"{len(df_validated):,} saved validations")
```

## Running improved matches and target specific industries:
```
#!python
import pandas as pd
from survey_companies import company_prep, read_write

# Load the master company db and prep the fuzzy library
o_Matcher = company_prep.CompanyMatcher(
    # path to excel file with master db sheet
    "Master List.xls",
    
    # optional path to saved validation file with "Account Name", "Alternate", "Confirm", "Version" columns
    saved_validations="validated_company_matches_updated.xlsx",
    
    # optional list of industries that survey is expected to contain. ["Healthcare Services", "Healthcare Software", "Hospitals & Physicians Clinics", ...],
    industries=[],
    
    # optional list of sic codes that survey is expected to contain. [6321, 5122, ...],
    sic_codes=[],
)
o_Matcher.create_fuzzy_matcher()

# load the batch of survey(s) and match the companies found in them
e_survey_matches, df_output = o_Matcher.process_survey(
    r"Participant list (1).xlsx",
    sheet_names=[  # exclude sheet_names to load all sheets in the file
        "McLagan Fin Tech",
        "Dietrich - Architecture",
        "Salary - IPAS Global Benchmark",
        "WMG Credit Unions",
        "PM - Cyber Security",
    ],
    non_industry_score_factor=0.75, # multiply match scores by this if they aren't part of the specified industries. No effect if no industries/sic codes specified
)

# generate a report on the accuracy of matching and herfindahl concentration of industry/sic code
df_report = o_Matcher.get_report(df_output)

# write the results and report to a workbook
o_writer = pd.ExcelWriter("output.xlsx")
df_output.to_excel(o_writer, sheet_name="Survey Companies", index=False)
df_report.to_excel(o_writer, sheet_name="Report", index=False)
o_writer.save()
o_writer.close()


# save the initial mappings to a validations file
df_validated = read_write.store_validations(
    e_validations=o_Matcher._validations,
    e_mapping_kinds=o_Matcher._mapping_kinds,
    e_mappings=o_Matcher._mappings,  #use None to not save unverified versions generated from master company DB
    df_validations=None,
    s_output="validated_company_matches_updated.xlsx")
print(f"{len(df_validated):,} saved validations")
```